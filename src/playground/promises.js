const promise = new Promise((resolve, reject) => {
	setTimeout( () => {
		// resolve({
		// 	name: 'Luis',
		// 	age: 25
		// });
		reject('Something went wrong!');
	}, 5000);
});

promise.then(
	(data) => {
		console.log('1', data);
	}/*, (err) => {
	console.log('error: ', err);
	}*/
).catch((err) => {
	console.log('error: ', err);
});