//
// Object Destructuring
//

// const person = {
// 	// name: 'Luis',
// 	age: 25,
// 	location: {
// 		city: 'Merida',
// 		temp: 25
// 	}
// };

// const {name: firstName = 'Anonymous', age} = person;

// console.log(`${firstName} is ${age}.`);

// const {city, temp: temperature} = person.location;

// if (city && temperature) {
// 	console.log(`It's ${temperature}°C in ${city}`);
// }

// const book = {
// 	title: 'Ego is he Enemy',
// 	author: 'Ryan Holiday',
// 	publisher: {
// 		//name: 'Penguin'
// 	}
// };

// const {name: publisherName = 'Self-Published'} = book.publisher;

// console.log(publisherName);

//
// Array destructuring
//

const address = ['12999 S Juniper Street', 'Merida', 'Venezuela', '5101'];
const [, city, country] = address;
console.log(`You are in ${city}, ${country}.`);

const item = ['Coffee (hot)', '$2.00', '$2.50', '$2.75'];
const [itemName, , mediumCost] = item;
console.log(`A medium ${itemName} cost ${mediumCost}`);