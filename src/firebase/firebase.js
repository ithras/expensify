import firebase from 'firebase';
import expenses from '../tests/fixtures/expenses';

const config = {
    apiKey: process.env.FIREBASE_API_KEY,
    authDomain: process.env.FIREBASE_AUTH_DOMAIN,
    databaseURL: process.env.FIREBASE_DATABASE_URL,
    projectId: process.env.FIREBASE_PROJECT_ID,
    storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID,
    appId: process.env.FIREBASE_APP_ID,
    measurementId: process.env.FIREBASE_MEASUREMENT_ID
};

// 
// Initialize Firebase
//

firebase.initializeApp(config);

const database = firebase.firestore();

const googleAuthProvider = new firebase.auth.GoogleAuthProvider();

export { database as default, firebase, googleAuthProvider };

// const expensesGet = [];

// database.collection('expenses')
// 	.get()
// 	.then( (docs) => {
// 		docs.forEach((snapshot) => {
// 			expensesGet.push({
// 				...snapshot.data(),
// 				'id': snapshot.id
// 			});
// 		})
// 		console.log(expensesGet);
// 	});



// database.collection('expenses').add(expenses[0]);
// database.collection('expenses').add(expenses[1]);
// database.collection('expenses').add(expenses[2]);

//
// Arrays
//

// const notes = [
// 	{
// 		id: '12',
// 		body: 'this is my note',
// 		title: 'first note'
// 	},
// 	{
// 		id: '32dd',
// 		body: 'this is my note',
// 		title: 'another note'
// 	}
// ];

// database.collection('notes').add({notes});

// const firebaseNotes = {
// 	'12': {
// 		body: 'this is my note',
// 		title: 'first note'
// 	},
// 	'32dd': {
// 		body: 'this is my note',
// 		title: 'another note'
// 	},
// 	ArrayExample: ['asd', false, 123]
// };

// database.collection('notes').doc('firestoreNotes').set(firebaseNotes);

// database.collection()
// 	.once('value')
// 	.then( (snapshot) => { 
// 		const val = snapshot.val();
// 		console.log(val);
// 	})
// 	.catch( (e) => {
// 		console.log('Error fetching data. ', e);
// 	});

// database.collection('users').add({
// 	username: 'Luis Garcia',
// 	age: 25,
// 	isSingle: true,
// 	stressLevel: 6,
// 	job: {
// 		title: 'Software Developer',
// 		company: 'Google'
// 	},
// 	location: {
// 		city: 'Merida',
// 		country: 'Venezuela'
// 	}
// }).then(() => {
// 	console.log('Data is saved!');
// }).catch((e) => {
// 	console.log('This failed', e);
// });

// database.collection('attributes').add({
// 	weigth: 88.5,
// 	heigth: 178
// }).then(() => {
// 	console.log('Data is updated!');
// }).catch((e) => {
// 	console.log('Didnt work: ', e);
// });

//
// Removing from firebase
//

// database.collection('isSingle').remove().then( () => {
// 	console.log('Removed suscesfully');
// }).catch((e) => {
// 	console.log('Error while removing: ', e);
// });

//
// Updating from firebase
//

// database.collection().update({
// 	username: 'Mike',
// 	age: 35,
// 	job: 'Software Developer',
// 	isSingle: null
// });

// database.collection().update({
// 	job: 'Manager',
// 	'location/city': 'Boston'
// });

// database.collection().update({
// 	stressLevel: 9,
// 	'location/city': 'Seattle',
// 	'job/company': 'Amazon'
// });

//
// Fetching data
//

// const onValueChange = database.collection('users').on('value', (snapshot) => { 
// 	const val = snapshot.val();
// 	console.log(val);
// }, (e) => {
// 	console.log('error with data fetching', e);
// });

// setTimeout(()=>{
// 	database.collection('age').add(46);
// }, 3500);

// setTimeout(()=>{
// 	database.collection().off('value',onValueChange);
// }, 7000);

// setTimeout(()=>{
// 	database.collection('age').add(30);
// }, 10500);