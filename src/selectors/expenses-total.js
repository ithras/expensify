export default (expenses) => {
	let result = [0];
	const reducer = (result, item) => result + item;

	expenses.map( (expense, index) => {
		result[index] = expense.amount;
	});

	return result.reduce(reducer);
};