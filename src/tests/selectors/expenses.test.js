import selectExpenses from '../../selectors/expenses';
import expenses from '../fixtures/expenses';
import moment from 'moment';

const filters = {
	text: '',
	sortBy: 'date',
	startDate: undefined,
	endDate: undefined
}

test('Should filter by text value', () => {
	const filterText = {
		...filters,
		text: 'e'
	}
	const result = selectExpenses(expenses, filterText);
	expect(result).toEqual([ expenses[2], expenses[1] ]);
});

test('Should filter by startDate', () => {
	const filterStartDate = {
		...filters,
		startDate: moment(0)
	};

	const result = selectExpenses(expenses, filterStartDate);
	expect(result).toEqual([expenses[2], expenses[0]]);
});

test('Should filer by endDate', () => {
	const filterEndDate = {
		...filters,
		endDate: moment(0)
	};
	const result = selectExpenses(expenses, filterEndDate);
	expect(result).toEqual([expenses[0], expenses[1]]);
});

test('Should sort by date', () => {
	const result = selectExpenses(expenses, filters);
	expect(result).toEqual([expenses[2], expenses[0], expenses[1]]);
});

test('Should sort by amount', () => {
	const filterSortAmount = {
		...filters,
		sortBy: 'amount'
	};
	const result = selectExpenses(expenses, filterSortAmount);
	expect(result).toEqual([expenses[1], expenses[2], expenses[0]]);
})