import getExpensesTotal from '../../selectors/expenses-total';
import expenses from '../fixtures/expenses';

const total = getExpensesTotal(expenses);

test('Should return 0 if no expenses', () => {
	expect(getExpensesTotal([])).toBe(0);
});

test('Should correctly add up a single expense', () => {
	expect(getExpensesTotal([expenses[2]])).toBe(expenses[2].amount);
});

test('Should correctly add up a list of expenses', () => {
	expect(getExpensesTotal(expenses)).toBe(total);
});