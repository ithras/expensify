import React from 'react';
import { shallow } from 'enzyme';
import { LoginPage } from '../../components/LoginPage.jsx';

let wrapper, startLogin;

beforeEach(() => {
	startLogin = jest.fn();
	wrapper = shallow(<LoginPage startLogin={startLogin}/>);
});

test('Should render LoginPage correctly', () => {
	expect(wrapper).toMatchSnapshot();
});

test('Should call logout on button click', () => {
	wrapper.find('form').simulate('submit', {
		preventDefault: () => {}
	});
	expect(startLogin).toHaveBeenCalled();
});