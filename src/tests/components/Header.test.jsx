import React from 'react';
import { shallow } from 'enzyme';
import { Header } from '../../components/Header.jsx';

let wrapper, startLogout;

beforeEach(() => {
	startLogout = jest.fn();
	wrapper = shallow(<Header startLogout={startLogout}/>);
});

test('Should render Header correctly', () => {
	expect(wrapper).toMatchSnapshot();

	//expect(wrapper.find('h1').text()).toBe('Expensify');
	// const renderer = new reactShallowRenderer();
	// renderer.render(<Header />);
	// expect(renderer.getRenderOutput()).toMatchSnapshot();
});

test('Should call logout on button click', () => {
	wrapper.find('button').simulate('click');
	expect(startLogout).toHaveBeenCalled();
});