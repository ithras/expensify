import React from 'react';
import { shallow } from 'enzyme';
import ExpenseForm from '../../components/ExpenseForm.jsx';
import expenses from '../fixtures/expenses';
import moment from 'moment';
import { SingleDatePicker } from 'react-dates';

test('Should render ExpenseForm correcly', () => {
	const wrapper = shallow(<ExpenseForm />);
	expect(wrapper).toMatchSnapshot();
});

test('Should render ExpenseForm with expense data', () => {
	const wrapper = shallow(<ExpenseForm expense={expenses[1]}/>);
	expect(wrapper).toMatchSnapshot();
});

test('Should render error for invalid form submission', () => {
	const wrapper = shallow(<ExpenseForm />);
	expect(wrapper).toMatchSnapshot();

	wrapper.find('form').simulate('submit', {
		preventDefault: () => {}
	});
	expect(wrapper.state('error').length).toBeGreaterThan(0);
	expect(wrapper).toMatchSnapshot();
});

test('Should set description on input change', () => {
	const value = 'New description';
	const wrapper = shallow(<ExpenseForm />);
	wrapper.find('input').at(0).simulate('change', {
		target: {
			value
		}
	});
	expect(wrapper.state('description')).toBe(value);
});

test('Should set note on textarea change', () => {
	const value = 'New Note';
	const wrapper = shallow(<ExpenseForm />);
	wrapper.find('textarea').simulate('change', {
		target: {
			value
		}
	});
	expect(wrapper.state('note')).toBe(value);
});

test('Should set amount to valid info', () => {
	const amount = '200.5';
	const wrapper = shallow(<ExpenseForm />);
	wrapper.find('input').at(1).simulate('change', {
		target: {
			value: amount
		}
	});
	expect(wrapper.state('amount')).toBe(amount);
});

test('Should NOT set amount to invalid info', () => {
	const amount = '12.122';
	const wrapper = shallow(<ExpenseForm />);
	wrapper.find('input').at(1).simulate('change', {
		target: {
			value: amount
		}
	});
	expect(wrapper.state('amount')).toBe('');
});

test('Should call onSubmit prop for valid form submission', () => {
	const onSubmitSpy = jest.fn();
	const wrapper = shallow(<ExpenseForm expense={expenses[2]} onSubmit={onSubmitSpy} />);
	wrapper.find('form').simulate('submit', {
		preventDefault: () => {}
	});
	expect(wrapper.state('error')).toBe('');
	expect(onSubmitSpy).toHaveBeenLastCalledWith(
		{
			description: expenses[2].description,
			amount: expenses[2].amount,
			note: expenses[2].note,
			createdAt: expenses[2].createdAt
		}
	);
});

test('Should set new date on date change', () => {
	const now = moment();
	const wrapper = shallow(<ExpenseForm />);
	wrapper.find(SingleDatePicker).prop('onDateChange')(now);
	expect(wrapper.state('createdAt')).toEqual(now);
});

test('Should set calendarFocused on focus change', () => {
	const focused = true;
	const wrapper = shallow(<ExpenseForm />);
	wrapper.find(SingleDatePicker).prop('onFocusChange')({ focused });
	expect(wrapper.state('calendarFocused')).toBe(focused);
});