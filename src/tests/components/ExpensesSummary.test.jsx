import React from 'react';
import { shallow } from 'enzyme';
import expenses from '../fixtures/expenses';
import { ExpensesSummary } from '../../components/ExpensesSummary.jsx';

test('Should render ExpensesSummary correctly With data', () => {
	const wrapper = shallow(<ExpensesSummary expenseCount={expenses.length} expensesTotal={114195} />);
	expect(wrapper).toMatchSnapshot();
});

test('Should render ExpensesSummary correctly Without data', () => {
	const wrapper = shallow(<ExpensesSummary expenseCount={0} expensesTotal={0} />);
	expect(wrapper).toMatchSnapshot();
});