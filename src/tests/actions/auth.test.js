import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import {login, logout} from '../../actions/auth';

const createMockStore = configureMockStore([thunk]);

test('Should setup login action correctly', () => {
	const action = login('314asd');
	expect(action).toEqual({
		type: 'LOGIN',
		uid: '314asd'
	});
});

test('Should setup logout action correctly', () => {
	const action = logout();
	expect(action).toEqual({
		type: 'LOGOUT'
	});
});