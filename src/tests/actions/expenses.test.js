import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import database from '../../firebase/firebase';
import { addExpense, 
	startAddExpense, 
	editExpense,
	startEditExpense,
	removeExpense,
	startRemoveExpense,
	setExpenses,
	startSetExpenses } from '../../actions/expenses';
import expenses from '../fixtures/expenses';

const createMockStore = configureMockStore([thunk]);

const uid = 'userTest';
const auth = {
	auth: {
		uid
	}
};
beforeEach((done) => {
	const expensesData = {};
	expenses.forEach(({id, description, note, amount, createdAt}) => {
		expensesData[id] = {description, note, amount, createdAt}
		database.collection('users').doc(uid)
				.collection('expenses').doc(`expense${id}`).set(expensesData[id]).then(() => done());
	});
	const getState = jest.fn();
});

//
// Remove Expense
//
test('Should setup remove expense action object', () => {
	const action = removeExpense({id: '123abc'});
	expect(action).toEqual({
		type: 'REMOVE_EXPENSE',
		id: '123abc'
	});
});

test('Should remove expense from firebase', (done) => {
	const store = createMockStore(auth);
	const id = expenses[2].id;
	store.dispatch(startRemoveExpense(`expense${id}`)).then( () => {
		const actions = store.getActions();
		expect(actions[0]).toEqual({
			type: 'REMOVE_EXPENSE',
			id: `expense${id}`
		});
		return database.collection('users').doc(uid)
						.collection('expenses').doc(`expense${id}`).get()
	}).then( (snapshot) => {
		expect(snapshot.data()).toBeFalsy();
		done();
	});
});

//
// Edit Expense
//
test('Should setup edit expense action object', () => {
	const action = editExpense('123abc', {note: 'New note value'});
	expect(action).toEqual({
		type: 'EDIT_EXPENSE',
		id: '123abc',
		updates: {
			note: 'New note value'
		}
	});
});

test('Should setup startEditExpense', (done) => {
	const store = createMockStore(auth);
	const id = expenses[2].id;
	const updates = { note: 'this has been updated' };
	store.dispatch(startEditExpense(`expense${id}`, updates)).then( () => {
		const actions = store.getActions();
		expect(actions[0]).toEqual({
			type: 'EDIT_EXPENSE',
			id: `expense${id}`,
			updates
		});

		return database.collection('users').doc(uid)
						.collection('expenses').doc(`expense${id}`).get();
	}).then((doc) => {
		expect(doc.data().note).toBe('this has been updated');
		done();
	});
});

//
// Add Expense
//
test('Should setup add expense action object with provided values', () => {
	const action = addExpense(expenses[0]);
	expect(action).toEqual({
		type: 'ADD_EXPENSE',
		expense: expenses[0]
	});
});

test('Should add expense to database and store', (done) => {
	const store = createMockStore(auth);
	const expenseData = {
		description: 'Mouse',
		amount: 3000,
		note: 'this one is better',
		createdAt: 25000
	}
	store.dispatch(startAddExpense(expenseData)).then( () => {
		const actions = store.getActions();
		expect(actions[0]).toEqual({
			type: 'ADD_EXPENSE',
			expense: {
				id: expect.any(String),
				...expenseData
			}
		});

		return database.collection('users').doc(uid)
						.collection('expenses').doc(actions[0].expense.id).get();
	}).then( (doc) => {
		expect(doc.data()).toEqual(expenseData);
		done();
	});
});

test('Should add expense with defaults to database and store', (done) => {
	const store = createMockStore(auth);
	const expenseData = {
		description: '', 
		note: '', 
		amount: 0, 
		createdAt: 0
	}

	store.dispatch(startAddExpense()).then( (expenseObj) => {
		const actions = store.getActions();
		expect(actions[0]).toEqual({
			type: 'ADD_EXPENSE',
			expense: {
				id: expect.any(String),
				...expenseData
			}
		});

		return database.collection('users').doc(uid)
						.collection('expenses').doc(actions[0].expense.id).get();
	}).then( (doc) => {
		expect(doc.data()).toEqual(expenseData);
		done();
	});
});

//
// Set Expense
//
test('Should setup set expense action object with data', () => {
	const action = setExpenses(expenses);
	expect(action).toEqual({
		type: 'SET_EXPENSES',
		expenses
	});
});

test('Should fetch the expenses from firebase', (done) => {
	const store = createMockStore(auth);
	const expenses = [];
	database.collection('users').doc('test')
			.collection('expenses').get().then((snapshot) => {
				snapshot.forEach((doc) => {
					expenses.push(doc.data());
				});
				done();
			});
	store.dispatch(startSetExpenses()).then(() => {
		const actions = store.getActions();
		expect(actions).toEqual({
			type: 'SET_EXPENSES',
			expenses
		});
		done();
	});
});