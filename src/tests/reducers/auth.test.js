import authReducer from '../../reducers/auth';

test('Should login user with uid', () => {
	const action = {
		type: 'LOGIN',
		uid: '314asd'
	}
	const state = authReducer({}, action);
	expect(state).toEqual({
		uid: action.uid
	});
});

test('Should logout user with uid', () => {
	const action = {
		type: 'LOGOUT'
	}
	const state = authReducer({uid: '314asd'}, action);
	expect(state).toEqual({});
});