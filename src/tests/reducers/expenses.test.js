import expensesReducer from '../../reducers/expenses';
import expenses from '../fixtures/expenses';

test('Should set default state', () => {
	const state = expensesReducer(undefined, {type:'@@INIT'});
	expect(state).toEqual([]);
}); 

test('Should remove expense by id',() => {
	const action = {
		type: 'REMOVE_EXPENSE',
		id: expenses[1].id
	};
	const state = expensesReducer(expenses, action);
	expect(state).toEqual([expenses[0], expenses[2]]);
});

test('Should not remove expense if id not found', () => {
	const action = {
		type: 'REMOVE_EXPENSE',
		id: '-1'
	};
	const state = expensesReducer(expenses, action);
	expect(state).toEqual(expenses);
});

test('Should add expense', () => {
	const expense = {
		id: '4',
		description: 'Keyboard',
		note: 'This is a keyboard',
		amount: 339500,
		createdAt: 5555
	}
	const action = {
		type: 'ADD_EXPENSE',
		expense
	};
	const state = expensesReducer(expenses, action);
	expect(state).toEqual([...expenses, expense]);
});

test('Should edit expense', () => {
	const updates = {
		note: 'last month rent'
	}
	const action = {
		type: 'EDIT_EXPENSE',
		id: expenses[1].id,
		updates
	}
	const state = expensesReducer(expenses, action);
	expect(state[1].note).toBe('last month rent');
});

test('Should NOT edit expense if id not found', () => {
	const updates = {
		note: 'last month rent'
	}
	const action = {
		type: 'EDIT_EXPENSE',
		id: '-1',
		updates
	}
	const state = expensesReducer(expenses, action);
	expect(state).toEqual(expenses);
});

test('Should set expenses', () => {
	const action = {
		type: 'SET_EXPENSES',
		expenses: [expenses[1]]
	}
	const state = expensesReducer(expenses, action);
	expect(state).toEqual([expenses[1]]);
});